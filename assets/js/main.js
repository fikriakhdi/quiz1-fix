$(document).ready(function(){
    if($.cookie("iduser")) $("#logoutbut").addClass("enabled")
    else $("#logoutbut").removeClass("enabled")
    $.getJSON('api/read_message', function (data) {
        var result = jQuery.parseJSON(JSON.stringify(data))
        if (result.status == 200) {
            
            var loop = "";
            $.map( result.data, function( o, i ) {
                var template = `<li class="media">
                <a class="pull-left">
                    <img src="assets/img/avatar.png" class="img-circle profile-pic">
                </a>
                <div class="media-body">
                    <span class="text-muted pull-right">
                        <small class="text-muted">Posted at `+o.created+` By <span class="text-danger">`+o.fullname+`</span></small>
                    </span>
                    
                    <p>`+o.content+`</p>
                </div>
                </li>`;
                console.log(template)
                loop+=template
              });
            
                $("#comments_pos").html(loop)
        } else {
            alert('failed to load data')
        }
    })
})
$("#submut_message").click(function(){
    var fullname = $.cookie("fullname");
    var userId = $.cookie("userid");
    var comment = $("#content").val();
    var element = $("#fullname")
    var today = new Date(Date.now());
    var year = today.getFullYear()
    var month = today.getMonth() + 1
    var day = today.getDate()
    var hours = today.getHours()
    var minutes = today.getMinutes()
    var seconds = today.getSeconds()
    var full_date = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds
    var fullname_val = element.val()
    if(fullname_val && comment){
    var template = `<li class="media">
    <a class="pull-left">
        <img src="assets/img/avatar.png" class="img-circle profile-pic">
    </a>
    <div class="media-body">
        <span class="text-muted pull-right">
            <small class="text-muted">Posted at `+full_date+` By <span class="text-danger">`+fullname_val+`</span></small>
        </span>
        
        <p>`+comment+`</p>
    </div>
    </li>`;
    if(fullname && userId){
        //send comment without registering
        $.cookie("fullname", fullname_val);
        var data = {userId:userId,content:comment}
        $.post('api/user/'+userId+"/comments",data,
            function (ret) {
                var result = JSON.parse(ret)
                if (result.status == 200) {
                    $("#content").val("")
                    setTimeout(function() {
                        $("#comments_pos").prepend(template)
                    }, 500);
                } else {
                    alert('failed to save comment');
                }
        })
    } else {
        //registering
        $.cookie("fullname", fullname_val)
        var data = {fullname:fullname_val,content:comment}
        $.post('api/register',data,
            function (ret) {
                var result = JSON.parse(ret)
                if (result.status == 200) {
                    $("#content").val("")
                    $("#fullname").attr("disabled", true)
                    $("#logoutbut").attr("disabled", false)
                    $("#logoutbut").val("Logout "+fullname_val+" ?")
                    setTimeout(function() {
                        $("#comments_pos").prepend(template)
                    }, 500);
                } else {
                    alert('failed to save comment');
                }
        })
    }
    } else alert('Please fill all the fields')
})



$(document).delegate("#logoutbut", "click", function (e) {
    e.preventDefault()
    $.cookie("fullname", "", -1)
    $.cookie("userid", "", -1)
    $("#fullname").val("")
    $("#fullname").removeAttr("readonly")
    $("#logoutbut").removeClass("enabled")
})

$(function() {

    var saveComment = function(data) {
            // Convert pings to human readable format
            $(Object.keys(data.pings)).each(function(index, userId) {
                var fullname = data.pings[userId];
                var pingText = '@' + fullname;
                data.content = data.content.replace(new RegExp('@' + userId, 'g'), pingText);
            });

            return data;
          }
          
          $('#comments-container').comments({
                textareaPlaceholderText: 'Yorum Yap',
                newestText: 'En Yeniler',
                oldestText: 'Son Eklenen',
                popularText: 'Popüler',
                sendText: 'Gönder',
                replyText: 'Yanıtla',
                editText: 'Düzenle',
                editedText: 'Düzenlendi',
                youText: 'Sen',
                saveText: 'Kaydet',
                logOutText: 'Çıkış Yap',
                deleteText: 'Sil',
                newText: 'Yeni',
                viewAllRepliesText: '__replyCount__ yanıtları görüntüle',
                hideRepliesText: 'Yanıtları Gizle',
                noCommentsText: 'Yorum Yapılmadı',

                profilePictureURL: 'https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png',
                currentUserId: $.cookie("userId"),
                roundProfilePictures: true,
                textareaRows: 1,
                enableHashtags: true,
                getComments: function(success, error) {
                var userId = $.cookie("userid");
                if(userId){
                  $.getJSON('api/user/'+userId+'/comments', function (data) {
                      var result = jQuery.parseJSON(JSON.stringify(data))
                      if (result.status == 200) {
                          setTimeout(function() {
                              console.log(result)
                          success(result.data);
                          }, 500);
                      } else {
                          alert('failed to load data')
                      }
                  })
                } else {
                    $.getJSON('api/read_message', function (data) {
                        var result = jQuery.parseJSON(JSON.stringify(data))
                        if (result.status == 200) {
                            setTimeout(function() {
                                console.log(result)
                            success(result.data);
                            }, 500);
                        } else {
                            alert('failed to load data')
                        }
                    })
                }
                },
                postComment: function(data, success, error) {
                  var userId = $.cookie("userid");
                  var fullname = $.cookie("fullname");
                  var fullname_val = $("#fullname").val();
                  if(userId && fullname){
                  $.post('api/user/'+userId+'/comments',data,
                  function (ret) {
                      var result = JSON.parse(ret)
                      if (result.status == 200) {
                          setTimeout(function() {
                          success(saveComment(data));
                          }, 500);} else {
                          alert('failed to save comment');
                      }
                      })
                    } else {
                        if(data.content && fullname_val){
                        //registering
                        $.cookie("fullname", fullname_val)
                        var data_in = {fullname:fullname_val,data:data}
                        $.post('api/register',data_in,
                            function (ret) {
                                var result = JSON.parse(ret)
                                if (result.status == 200) {
                                    $.cookie("userid", result.userid)
                                    $("#content").val("")
                                    $("#fullname").attr("readonly", true)
                                    $("#logoutbut").addClass("enabled")
                                    setTimeout(function() {
                                        success(saveComment(data));
                                    }, 500);
                                } else {
                                    alert('failed to save comment');
                                }
                        })
                    } else {
                        if(fullname_val==""){
                        $("#fullname").focus()
                        alert('Please input your name')
                        } else if(data.content==""){
                            $(".textarea").focus()
                            alert('Please input your message')
                        } else 
                        alert('Please input all fields')
                    }
                }  
                },
                upvoteComment: function(data, success, error) {
                  var userId = $.cookie("userid");
                  if(userId){
                  $.ajax({
                      url:'api/user/'+userId+'/comments',
                      type:"PUT",
                      data:data,
                      success:function (ret) {
                      var result = JSON.parse(ret)
                      if (result.status == 200) {
                          setTimeout(function() {
                          success(data);
                          }, 500);
                      } else {
                          alert('failed to like comment');
                      }
                      }})
                    } else alert('you have to login first')
                },
              });
          });