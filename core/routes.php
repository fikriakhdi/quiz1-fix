<?php
//basic routing, by exploding every part of URL
$link = preg_replace('[^a-z0-9-]','',$_SERVER['REQUEST_URI']);
if ($_SERVER['HTTP_HOST'] == '127.0.0.1' or $_SERVER['HTTP_HOST'] == 'localhost') { $link = substr($link,1,200); };
$url  = (explode('/',$link));
$url_1 	= isset($url[1]) ? $url[1] : '';
$url_2 	= isset($url[2]) ? $url[2] : '';
$url_3  = isset($url[3]) ? $url[3] : '';
$url_4 	= isset($url[4]) ? $url[4] : '';
$url_5 	= isset($url[5]) ? $url[5] : '';
$url_6 	= isset($url[6]) ? $url[6] : '';

if($url_1=="api"){
    if($url_2=="user"){
        if($url_4=="comments"){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $customer_id = $url_3;
                include "api/post_comment.php";
            } 
            if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
                $customer_id = $url_3;
                include "api/like_comment.php";
            } 
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $customer_id = $url_3;
                include "api/get_comment.php";
            } 
    } 
    } else if($url_2=="read_message"){
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        include "api/get_comment.php";
        }
    } else if($url_2=="register"){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            include "api/register.php";
        } 
    } 
} else if($url_1==""){
    include "core/index.php";
} 
?>