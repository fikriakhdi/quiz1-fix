<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Real-Time Comments Software By Fikri</title>
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/jquery-comments.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
			body {
				padding: 20px;
				margin: 0px;
				font-size: 14px;
				font-family: "Arial", Georgia, Serif;
			}
		</style>
</head>
<body>
    <main role="main" class="container">

        <div class="starter-template">
            <h3>Real-Time Comments By Fikri</h3>
            <div id="danger-msg" class="alert alert-danger" style="display: none;"></div>
            <div class="form-group">
                <input type="text" id="fullname" value="<?php echo (!empty($_COOKIE['fullname'])?$_COOKIE['fullname']:"");?>" class="form-control" placeholder="Full Name" <?php echo (!empty($_COOKIE['fullname'])?"readonly":"");?>>
            </div>
            <div class="clearfix m-4"></div>
            <ul class="media-list">
            <div id="comments-container"></div>
            </ul>
        </div>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/jquery-comments.js"></script>
    <script>


    </script>
